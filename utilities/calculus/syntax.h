/*
 * Automatically generated from the files:
 *	syntax.sid
 * and
 *	syntax.act
 * by:
 *	sid
 */

/* BEGINNING OF HEADER */


/*
    		 Crown Copyright (c) 1997
    
    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-
    
        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;
    
        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;
    
        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;
    
        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/


#ifndef SYNTAX_INCLUDED
#define SYNTAX_INCLUDED


/* BEGINNING OF FUNCTION DECLARATIONS */

extern void extra_calculus(void);
extern void read_calculus(void);

/* BEGINNING OF TERMINAL DEFINITIONS */

#define lex_number (1)
#define lex_lshift (35)
#define lex_open_Hround (38)
#define lex_comma (27)
#define lex_rshift (44)
#define lex_unknown (48)
#define lex_stack (17)
#define lex_string (2)
#define lex_extras (8)
#define lex_equal (32)
#define lex_colon (25)
#define lex_compl (28)
#define lex_import (7)
#define lex_unions (13)
#define lex_hash (34)
#define lex_struct (5)
#define lex_identifier (0)
#define lex_union (6)
#define lex_arrow (21)
#define lex_minus (36)
#define lex_maps (10)
#define lex_and (20)
#define lex_enum (4)
#define lex_identities (9)
#define lex_eof (31)
#define lex_star (46)
#define lex_with (14)
#define lex_list (15)
#define lex_vec (18)
#define lex_div (29)
#define lex_vec_Hptr (19)
#define lex_rem (43)
#define lex_plus (41)
#define lex_close_Hbrace (22)
#define lex_dot (30)
#define lex_semicolon (45)
#define lex_algebra (3)
#define lex_primitives (11)
#define lex_ptr (16)
#define lex_xor (47)
#define lex_close_Hsquare (24)
#define lex_or (40)
#define lex_open_Hbrace (37)
#define lex_exclaim (33)
#define lex_colon_Hcolon (26)
#define lex_structures (12)
#define lex_close_Hround (23)
#define lex_open_Hsquare (39)
#define lex_question (42)

/* BEGINNING OF TRAILER */


#endif

/* END OF FILE */
