/*
    		 Crown Copyright (c) 1997
    
    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-
    
        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;
    
        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;
    
        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;
    
        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/

/*
 *  AUTOMATICALLY GENERATED BY lexi VERSION 1.2
 */


/* KEYWORDS */

MAKE_KEYWORD("ALGEBRA", lex_algebra);
MAKE_KEYWORD("LIST", lex_list);
MAKE_KEYWORD("PTR", lex_ptr);
MAKE_KEYWORD("STACK", get_stack());
MAKE_KEYWORD("VEC", get_vec());
MAKE_KEYWORD("VEC_PTR", get_vec_ptr());
if (!new_format) {
    MAKE_KEYWORD("EXTRAS", lex_extras);
    MAKE_KEYWORD("IDENTITIES", lex_identities);
    MAKE_KEYWORD("MAPS", lex_maps);
    MAKE_KEYWORD("PRIMITIVES", lex_primitives);
    MAKE_KEYWORD("STRUCTURES", lex_structures);
    MAKE_KEYWORD("UNIONS", lex_unions);
    MAKE_KEYWORD("WITH", lex_with);
}
if (new_format) {
    MAKE_KEYWORD("IMPORT", lex_import);
    MAKE_KEYWORD("enum", lex_enum);
    MAKE_KEYWORD("struct", lex_struct);
    MAKE_KEYWORD("union", lex_union);
}
