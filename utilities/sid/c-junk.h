/*
 * Automatically generated from the files:
 *	c-parser.sid
 * and
 *	c-parser.act
 * by:
 *	./obj/sid
 */

/* BEGINNING OF HEADER */


/*
    		 Crown Copyright (c) 1997
    
    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-
    
        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;
    
        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;
    
        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;
    
        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/



/* BEGINNING OF FUNCTION DECLARATIONS */

extern void c_parse_grammar (void);

/* BEGINNING OF TERMINAL DEFINITIONS */

#define ZBopen_Htuple (18)
#define ZBsid_Hidentifier (8)
#define ZBcode (16)
#define ZBblt_Hmaps (1)
#define ZBblt_Hassignments (2)
#define ZBarrow (17)
#define ZBreference (21)
#define ZBbegin_Haction (13)
#define ZBc_Hidentifier (9)
#define ZBeof (22)
#define ZBblt_Hparam_Hassign (20)
#define ZBblt_Hterminals (3)
#define ZBblt_Hresult_Hassign (7)
#define ZBterminator (12)
#define ZBseparator (10)
#define ZBblt_Hprefixes (0)
#define ZBblt_Hheader (4)
#define ZBblt_Hactions (5)
#define ZBend_Haction (15)
#define ZBblt_Htrailer (6)
#define ZBdefine (14)
#define ZBtypemark (11)
#define ZBclose_Htuple (19)

/* BEGINNING OF TRAILER */



/* END OF FILE */
