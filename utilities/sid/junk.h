/*
 * Automatically generated from the files:
 *	parser.sid
 * and
 *	parser.act
 * by:
 *	./sid
 */

/* BEGINNING OF HEADER */


/*
    		 Crown Copyright (c) 1997
    
    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-
    
        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;
    
        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;
    
        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;
    
        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/



/* BEGINNING OF FUNCTION DECLARATIONS */

extern void sid_parse_grammar(void);

/* BEGINNING OF TERMINAL DEFINITIONS */

#define ZBpred_Hresult (18)
#define ZBend_Hscope (22)
#define ZBignore (19)
#define ZBopen_Htuple (6)
#define ZBend_Hrule (14)
#define ZBblt_Hentry (3)
#define ZBblt_Htypes (0)
#define ZBidentifier (4)
#define ZBarrow (8)
#define ZBempty (17)
#define ZBblt_Hproductions (2)
#define ZBreference (24)
#define ZBbegin_Haction (10)
#define ZBalt_Hsep (15)
#define ZBeof (25)
#define ZBblt_Hterminals (1)
#define ZBscopemark (20)
#define ZBterminator (9)
#define ZBbegin_Hscope (21)
#define ZBseparator (23)
#define ZBhandler_Hsep (16)
#define ZBend_Haction (11)
#define ZBdefine (12)
#define ZBbegin_Hrule (13)
#define ZBtypemark (5)
#define ZBclose_Htuple (7)

/* BEGINNING OF TRAILER */



/* END OF FILE */
