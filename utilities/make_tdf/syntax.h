/*
 * Automatically generated from the files:
 *	syntax.sid
 * and
 *	syntax.act
 * by:
 *	sid
 */

/* BEGINNING OF HEADER */


/*
    		 Crown Copyright (c) 1997
    
    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-
    
        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;
    
        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;
    
        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;
    
        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/


#ifndef SYNTAX_INCLUDED
#define SYNTAX_INCLUDED


/* BEGINNING OF FUNCTION DECLARATIONS */

extern void read_spec(SPECIFICATION *);

/* BEGINNING OF TERMINAL DEFINITIONS */

#define lex_break (2)
#define lex_number (1)
#define lex_edge_Hsort (7)
#define lex_comma (29)
#define lex_kinds_Hof_Hunit (14)
#define lex_options (19)
#define lex_edge_Hconstructor (6)
#define lex_unknown (31)
#define lex_sortid (23)
#define lex_close (28)
#define lex_entity_Hsort (11)
#define lex_unit_Hidentifier (26)
#define lex_result_Hsort (21)
#define lex_name (0)
#define lex_slists (22)
#define lex_encoding_Hbits (9)
#define lex_lists (16)
#define lex_boundaries (3)
#define lex_parameter_Hsorts (20)
#define lex_open (27)
#define lex_eof (30)
#define lex_construct_Hname (5)
#define lex_sorts (24)
#define lex_encoding (8)
#define lex_graph_Hedges (12)
#define lex_has_Hextension (13)
#define lex_unit_Helements (25)
#define lex_major_Hversion (17)
#define lex_constructs (4)
#define lex_linkable_Hentities (15)
#define lex_minor_Hversion (18)
#define lex_entity_Hidentifier (10)

/* BEGINNING OF TRAILER */


#endif

/* END OF FILE */
