/*
 * Automatically generated from the files:
 *	syntax.sid
 * and
 *	syntax.act
 * by:
 *	sid
 */

/* BEGINNING OF HEADER */


/*
    		 Crown Copyright (c) 1997

    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-

        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;

        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;

        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;

        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/


#ifndef SYNTAX_INCLUDED
#define SYNTAX_INCLUDED


/* BEGINNING OF FUNCTION DECLARATIONS */

extern void read_errors(void);

/* BEGINNING OF TERMINAL DEFINITIONS */

#define lex_entries (4)
#define lex_open_Hround (20)
#define lex_comma (17)
#define lex_prefix (7)
#define lex_alt_Hname (2)
#define lex_comp_Houtput (12)
#define lex_unknown (25)
#define lex_from_Hcomp (13)
#define lex_db_Hname (3)
#define lex_usage (11)
#define lex_string (1)
#define lex_equal (18)
#define lex_colon (16)
#define lex_identifier (0)
#define lex_from_Hdb (14)
#define lex_arrow (15)
#define lex_types (10)
#define lex_eof (24)
#define lex_keys (6)
#define lex_rig (9)
#define lex_close_Hbrace (23)
#define lex_key (5)
#define lex_properties (8)
#define lex_or (19)
#define lex_open_Hbrace (22)
#define lex_close_Hround (21)

/* BEGINNING OF TRAILER */


#endif

/* END OF FILE */
