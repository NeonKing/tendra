Directory Restructuring
=======================

This document describes the directory restructuring proposed in the
./outlineplan and ./components documents.

Paths are approximate, and do not include the trunk or src components,
for brevity.

Old path                    Component    New path
========================================================================

/tendra                     libTenDRA    /tendra
/tdf                        libANDF      /andf

/tendra/installers          Installers   /tendra/installers
/tendra/producers           Producers    /tendra/producers

Tools:
/tendra/tools/disp          disp         /disp
/tendra/tools/tcc           tcc          /tcc
/tendra/tools/tld           tld          /tld
/tendra/tools/tnc           tnc          /tnc
/tendra/tools/tpl           tpl          /tpl
/tendra/utilities/lexi      lexi         /lexi
/tendra/utilities/sid       sid          /sid
/tendra/utilities/calculus  calculus     /calculus

Utilities:
/tendra/tools/tspec         tspec        /tendra/utilities/tspec
/tendra/utilities/make_err  make_err     /tendra/utilities/make_err
/tendra/utilities/make_tdf  make_tdf     /andf/utilities/make_tdf

Miscellany:
/tendra/lib/apis            Producers    /tendra/producers/lib/apis
/tendra/lib/env             tcc          /tcc/lib/env
/tendra/lib/machines        tcc          /tcc/lib/machines
/tendra/lib/tdf             libANDF      /andf/lib/tdf
/tendra/lib/cpp             tcc          /tcc/lib/cpp
/tendra/lib/libtdf          libANDF      /andf/lib/libtdf
/tendra/lib/startup         libTenDRA    /tendra/lib/startup

See development/movingouttools for details of how we intend to go about
moving the tools into their new paths, and how they relate to the
shared components with the rest of the source tree.

Within these directories, the structure is expected to remain as-is.

Directories not mentioned here are special cases, and need splitting
up according to their own specific requirements.

This document does not cover the build system; see
development/buildsystem instead.
