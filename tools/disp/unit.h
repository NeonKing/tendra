/*
 * Copyright (c) 2002-2005 The TenDRA Project <http://www.tendra.org/>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of The TenDRA Project nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific, prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS
 * IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 */
/*
    		 Crown Copyright (c) 1997

    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-

        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;

        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;

        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;

        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/


#ifndef UNIT_INCLUDED
#define UNIT_INCLUDED


/*
    TOKEN/TAG DECLARATIONS/DEFINITIONS

    This file contains the routines for decoding token declarations, token
    definitions, tag declarations, tag definitions and the standard unit
    types associated with them.
*/

extern void token_sort(object *, sortname, char *, long);
extern void read_no_labs(void);
extern void de_tagdec_props(void);
extern void de_tagdef_props(void);
extern void de_tokdec_props(void);
extern void de_tokdef_props(void);
extern void de_al_tagdef_props(void);
extern void de_dg_comp_props(void);
extern void de_diag_type_unit(void);
extern void de_diag_unit(void);
extern void de_linkinfo_props(void);
extern void de_version_props(void);
extern void de_tld2_unit(void);
extern void de_tld_unit(void);
extern void de_make_version(char *);
extern void de_magic(char *);
extern long max_lab_no;
extern int show_usage;
extern int diagnostics;
extern int versions;


/*
    UNIT USAGE INFORMATION

    These macros describe the units decoded by the routines above and
    give the flags which control their use.
*/

#define MSG_tagdec_props		"TAG DECLARATIONS"
#define MSG_tagdef_props		"TAG DEFINITIONS"
#define MSG_tokdec_props		"TOKEN DECLARATIONS"
#define MSG_tokdef_props		"TOKEN DEFINITIONS"
#define MSG_al_tagdef_props		"ALIGNMENT TAG DEFINITIONS"
#define MSG_dg_comp_props		"DIAGNOSTIC INFORMATION"
#define MSG_diag_type_unit		"DIAGNOSTIC TYPE INFORMATION"
#define MSG_diag_unit			"DIAGNOSTIC INFORMATION"
#define MSG_linkinfo_props		"LINKING INFORMATION"
#define MSG_version_props		"TDF VERSION NUMBER"
#define MSG_tld2_unit			"LINKER INFORMATION"
#define MSG_tld_unit			"LINKER INFORMATION"

#define OPT_tagdec_props		1
#define OPT_tagdef_props		1
#define OPT_tokdec_props		1
#define OPT_tokdef_props		1
#define OPT_al_tagdef_props		1
#define OPT_dg_comp_props		diagnostics
#define OPT_diag_type_unit		diagnostics
#define OPT_diag_unit			diagnostics
#define OPT_linkinfo_props		show_usage
#define OPT_version_props		versions
#define OPT_tld2_unit			show_usage
#define OPT_tld_unit			show_usage


#endif
