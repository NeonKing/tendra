/*
    		 Crown Copyright (c) 1997
    
    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-
    
        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;
    
        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;
    
        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;
    
        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/


#pragma TenDRA begin
#pragma TenDRA implicit function declaration off
#pragma TenDRA incompatible void return disallow
#pragma TenDRA unreachable code disallow
#pragma TenDRA fall into case disallow
#pragma TenDRA conversion analysis (int-pointer) on
#pragma TenDRA conversion analysis (pointer-pointer) on
#pragma TenDRA conversion analysis (int-int explicit) off
#pragma TenDRA conversion analysis (int-int implicit) warning
#pragma TenDRA function pointer as pointer disallow
#pragma TenDRA complete struct/union analysis off
#pragma TenDRA variable analysis warning
#pragma TenDRA discard analysis (function return) on
#pragma TenDRA discard analysis (value) on
#pragma TenDRA discard analysis (static) on
#pragma TenDRA linkage resolution : off
#pragma TenDRA unknown pragma disallow
#pragma TenDRA directive file disallow
#pragma TenDRA directive ident disallow
#pragma TenDRA directive assert disallow
#pragma TenDRA directive unassert disallow
#pragma TenDRA directive weak disallow
#pragma TenDRA extra bitfield int type disallow
#pragma TenDRA conditional lvalue disallow
#pragma TenDRA no external declaration disallow
#pragma TenDRA weak macro equality disallow
#pragma TenDRA extra ; disallow
#pragma TenDRA extra type definition disallow
#pragma TenDRA implicit int type for external declaration disallow
#pragma TenDRA implicit int type for function return disallow
#pragma TenDRA includes depth 8
#pragma TenDRA indented # directive allow
#pragma TenDRA initialization of struct/union (auto) disallow
#pragma TenDRA text after directive disallow
#pragma TenDRA ignore struct/union/enum tag off
#pragma TenDRA unknown escape disallow
