#ifndef __HACKED_LANGINFO_INCLUDED
#define __HACKED_LANGINFO_INCLUDED

#include <nl_types.h>

#ifndef RADIXCHAR
#define RADIXCHAR	39
#endif

#ifndef THOUSEP
#define THOUSEP		40
#endif

#ifndef CRNCYSTR
#define CRNCYSTR	43
#endif

#endif
