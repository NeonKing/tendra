/*
    		 Crown Copyright (c) 1997
    
    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-
    
        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;
    
        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;
    
        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;
    
        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/
# Properties and atoms (4.3)

+USE "x5/lib", "X11/X.h" ;

+CONST Atom XA_PRIMARY, XA_SECONDARY ;

+CONST Atom XA_CUT_BUFFER0, XA_CUT_BUFFER1, XA_CUT_BUFFER2, XA_CUT_BUFFER3 ;
+CONST Atom XA_CUT_BUFFER4, XA_CUT_BUFFER5, XA_CUT_BUFFER6, XA_CUT_BUFFER7 ;
+CONST Atom XA_RGB_BEST_MAP, XA_RGB_BLUE_MAP, XA_RGB_DEFAULT_MAP ;
+CONST Atom XA_RGB_GRAY_MAP, XA_RGB_GREEN_MAP, XA_RGB_RED_MAP ;
+CONST Atom XA_RESOURCE_MANAGER, XA_WM_CLASS, XA_WM_CLIENT_MACHINE ;
+CONST Atom XA_WM_COLORMAP_WINDOWS, XA_WM_COMMAND, XA_WM_HINTS ;
+CONST Atom XA_WM_ICON_NAME, XA_WM_ICON_SIZE, XA_WM_NAME ;
+CONST Atom XA_WM_NORMAL_HINTS, XA_WM_PROTOCOLS, XA_WM_STATE ;
+CONST Atom XA_WM_TRANSIENT_FOR ;

+CONST Atom XA_ARC, XA_ATOM, XA_BITMAP, XA_CARDINAL, XA_COLORMAP ;
+CONST Atom XA_CURSOR, XA_DRAWABLE, XA_FONT, XA_INTEGER, XA_PIXMAP ;
+CONST Atom XA_POINT, XA_RGB_COLOR_MAP, XA_RECTANGLE, XA_STRING ;
+CONST Atom XA_VISUALID, XA_WINDOW, XA_WM_SIZE_HINTS ;

+CONST Atom XA_MIN_SPACE, XA_NORM_SPACE, XA_MAX_SPACE, XA_END_SPACE ;
+CONST Atom XA_SUPERSCRIPT_X, XA_SUPERSCRIPT_Y, XA_SUBSCRIPT_X ;
+CONST Atom XA_SUBSCRIPT_Y, XA_UNDERLINE_POSITION, XA_UNDERLINE_THICKNESS ;
+CONST Atom XA_FONT_NAME, XA_FULL_NAME, XA_STRIKEOUT_DESCENT ;
+CONST Atom XA_STRIKEOUT_ASCENT, XA_ITALIC_ANGLE, XA_X_HEIGHT ;
+CONST Atom XA_QUAD_WIDTH, XA_WEIGHT, XA_POINT_SIZE, XA_RESOLUTION ;
+CONST Atom XA_COPYRIGHT, XA_NOTICE, XA_FAMILY_NAME, XA_CAP_HEIGHT ;
