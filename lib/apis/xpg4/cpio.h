#   		 Crown Copyright (c) 1997
#   
#   This TenDRA(r) Computer Program is subject to Copyright
#   owned by the United Kingdom Secretary of State for Defence
#   acting through the Defence Evaluation and Research Agency
#   (DERA).  It is made available to Recipients with a
#   royalty-free licence for its use, reproduction, transfer
#   to other parties and amendment for any purpose not excluding
#   product development provided that any such use et cetera
#   shall be deemed to be acceptance of the following conditions:-
#   
#       (1) Its Recipients shall ensure that this Notice is
#       reproduced upon any copies or amended versions of it;
#   
#       (2) Any amended version of it shall be clearly marked to
#       show both the nature of and the organisation responsible
#       for the relevant amendment or amendments;
#   
#       (3) Its onward transfer from a recipient to another
#       party shall be deemed to be that party's acceptance of
#       these conditions;
#   
#       (4) DERA gives no warranty or assurance as to its
#       quality or suitability for any purpose and DERA accepts
#       no liability whatsoever in relation to any use to which
#       it may be put.
#
+IFNDEF C_IRUSR
+DEFINE C_IRUSR		%% 0000400 %% ;
+DEFINE C_IWUSR		%% 0000200 %% ;
+DEFINE C_IXUSR		%% 0000100 %% ;
+DEFINE C_IRGRP		%% 0000040 %% ;
+DEFINE C_IWGRP		%% 0000020 %% ;
+DEFINE C_IXGRP		%% 0000010 %% ;
+DEFINE C_IROTH		%% 0000004 %% ;
+DEFINE C_IWOTH		%% 0000002 %% ;
+DEFINE C_IXOTH		%% 0000001 %% ;
+DEFINE C_ISUID		%% 0004000 %% ;
+DEFINE C_ISGID		%% 0002000 %% ;
+DEFINE C_ISVTX		%% 0001000 %% ;
+DEFINE C_ISDIR		%% 0040000 %% ;
+DEFINE C_ISFIFO	%% 0010000 %% ;
+DEFINE C_ISREG		%% 0100000 %% ;
+DEFINE C_ISBLK		%% 0060000 %% ;
+DEFINE C_ISCHR		%% 0020000 %% ;
+DEFINE C_ISCTG		%% 0110000 %% ;
+DEFINE C_ISLNK		%% 0120000 %% ;
+DEFINE C_ISSOCK	%% 0140000 %% ;
+DEFINE MAGIC		%% "070707" %% ;
+ENDIF
