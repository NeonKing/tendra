/*
    Copyright (c) 1995 Open Software Foundation, Inc.

    All Rights Reserved

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notice appears in all copies and that both the
    copyright notice and this permission notice appear in supporting
    documentation.

    OSF DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
    INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE.

    IN NO EVENT SHALL OSF BE LIABLE FOR ANY SPECIAL, INDIRECT, OR
    CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN ACTION OF CONTRACT,
    NEGLIGENCE, OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/*
    COPYRIGHT NOTICE

    This program contains amendments to Motif 1.1 API headers in
    order to represent the Motif 1.2 API. These amendments are the
    property of IXI Ltd, a subsidiary of the Santa Cruz Operation (SCO).
    Use, reproduction, production of amended versions and/or transfer of
    this program is permitted PROVIDED THAT:

    (a)  This legend be preserved on any such reproduction and amended
         version.

    (b)  Any recipient of such reproduction or amended version accept
         the conditions set out in this legend.

    IXI accepts no liability whatsoever in relation to any use to
    which this program may be put and gives no warranty as to the
    program's suitability for any purpose.

    All rights reserved.

    Copyright (c) 1995, 1996
*/



/* SCO CID (IXI) MrmDecls.h,v 1.1 1996/08/08 14:12:01 wendland Exp */

/*
 * Non-documented features but defined by IXI implementation.
 */

+EXP (extern) lvalue char _MrmMsf_0000[];
+EXP (extern) lvalue char _MrmMsf_0001[];
+EXP (extern) lvalue char _MrmMsf_0002[];
+EXP (extern) lvalue char _MrmMsf_0003[];
+EXP (extern) lvalue char _MrmMsf_0004[];
+EXP (extern) lvalue char _MrmMsf_0005[];
+EXP (extern) lvalue char _MrmMsf_0006[];
+EXP (extern) lvalue char _MrmMsf_0007[];
+EXP (extern) lvalue char _MrmMsf_0008[];
+EXP (extern) lvalue char _MrmMsf_0009[];
+EXP (extern) lvalue char _MrmMsf_0010[];
+EXP (extern) lvalue char _MrmMsf_0011[];
+EXP (extern) lvalue char _MrmMsf_0012[];
+EXP (extern) lvalue char _MrmMsf_0013[];
+EXP (extern) lvalue char _MrmMsf_0014[];
+EXP (extern) lvalue char _MrmMsf_0015[];
+EXP (extern) lvalue char _MrmMsf_0016[];
+EXP (extern) lvalue char _MrmMsf_0017[];
+EXP (extern) lvalue char _MrmMsf_0018[];
+EXP (extern) lvalue char _MrmMsf_0019[];
+EXP (extern) lvalue char _MrmMsf_0020[];
+EXP (extern) lvalue char _MrmMsf_0021[];
+EXP (extern) lvalue char _MrmMsf_0022[];
+EXP (extern) lvalue char _MrmMsf_0023[];
+EXP (extern) lvalue char _MrmMsf_0024[];
+EXP (extern) lvalue char _MrmMsf_0025[];
+EXP (extern) lvalue char _MrmMsf_0026[];
+EXP (extern) lvalue char _MrmMsf_0027[];
+EXP (extern) lvalue char _MrmMsf_0028[];
+EXP (extern) lvalue char _MrmMsf_0029[];
+EXP (extern) lvalue char _MrmMsf_0030[];
+EXP (extern) lvalue char _MrmMsf_0031[];
+EXP (extern) lvalue char _MrmMsf_0032[];
+EXP (extern) lvalue char _MrmMsf_0033[];
+EXP (extern) lvalue char _MrmMsf_0034[];
+EXP (extern) lvalue char _MrmMsf_0035[];
+EXP (extern) lvalue char _MrmMsf_0036[];
+EXP (extern) lvalue char _MrmMsf_0037[];
+EXP (extern) lvalue char _MrmMsf_0038[];
+EXP (extern) lvalue char _MrmMsf_0039[];
+EXP (extern) lvalue char _MrmMsf_0040[];
+EXP (extern) lvalue char _MrmMsf_0041[];
+EXP (extern) lvalue char _MrmMsf_0042[];
+EXP (extern) lvalue char _MrmMsf_0043[];
+EXP (extern) lvalue char _MrmMsf_0044[];
+EXP (extern) lvalue char _MrmMsf_0045[];
+EXP (extern) lvalue char _MrmMsf_0046[];
+EXP (extern) lvalue char _MrmMsf_0047[];
+EXP (extern) lvalue char _MrmMsf_0048[];
+EXP (extern) lvalue char _MrmMsf_0049[];
+EXP (extern) lvalue char _MrmMsf_0050[];
+EXP (extern) lvalue char _MrmMsf_0051[];
+EXP (extern) lvalue char _MrmMsf_0052[];
+EXP (extern) lvalue char _MrmMsf_0053[];
+EXP (extern) lvalue char _MrmMsf_0054[];
+EXP (extern) lvalue char _MrmMsf_0055[];
+EXP (extern) lvalue char _MrmMsf_0056[];
+EXP (extern) lvalue char _MrmMsf_0057[];
+EXP (extern) lvalue char _MrmMsf_0058[];
+EXP (extern) lvalue char _MrmMsf_0059[];
+EXP (extern) lvalue char _MrmMsf_0060[];
+EXP (extern) lvalue char _MrmMsf_0061[];
+EXP (extern) lvalue char _MrmMsf_0062[];
+EXP (extern) lvalue char _MrmMsf_0063[];
+EXP (extern) lvalue char _MrmMsf_0064[];
+EXP (extern) lvalue char _MrmMsf_0065[];
+EXP (extern) lvalue char _MrmMsf_0066[];
+EXP (extern) lvalue char _MrmMsf_0067[];
+EXP (extern) lvalue char _MrmMsf_0068[];
+EXP (extern) lvalue char _MrmMsf_0069[];
+EXP (extern) lvalue char _MrmMsf_0070[];
+EXP (extern) lvalue char _MrmMsf_0071[];
+EXP (extern) lvalue char _MrmMsf_0072[];
+EXP (extern) lvalue char _MrmMsf_0073[];
+EXP (extern) lvalue char _MrmMsf_0074[];
+EXP (extern) lvalue char _MrmMsf_0075[];
+EXP (extern) lvalue char _MrmMsf_0076[];
+EXP (extern) lvalue char _MrmMsf_0077[];
+EXP (extern) lvalue char _MrmMsf_0078[];
+EXP (extern) lvalue char _MrmMsf_0079[];
+EXP (extern) lvalue char _MrmMsf_0080[];
+EXP (extern) lvalue char _MrmMsf_0081[];
+EXP (extern) lvalue char _MrmMsf_0082[];
+EXP (extern) lvalue char _MrmMsf_0083[];
+EXP (extern) lvalue char _MrmMsf_0084[];
+EXP (extern) lvalue char _MrmMsf_0085[];
+EXP (extern) lvalue char _MrmMsf_0086[];
+EXP (extern) lvalue char _MrmMsf_0087[];
+EXP (extern) lvalue char _MrmMsf_0088[];
+EXP (extern) lvalue char _MrmMsf_0089[];
+EXP (extern) lvalue char _MrmMsf_0090[];
+EXP (extern) lvalue char _MrmMsf_0091[];
+EXP (extern) lvalue char _MrmMsf_0092[];
+EXP (extern) lvalue char _MrmMsf_0093[];
+EXP (extern) lvalue char _MrmMsf_0094[];
+EXP (extern) lvalue char _MrmMsf_0095[];
+EXP (extern) lvalue char _MrmMsf_0096[];
+EXP (extern) lvalue char _MrmMsf_0097[];
+EXP (extern) lvalue char _MrmMsf_0098[];
+EXP (extern) lvalue char _MrmMsf_0099[];
+EXP (extern) lvalue char _MrmMsf_0100[];
+EXP (extern) lvalue char _MrmMsf_0101[];
+EXP (extern) lvalue char _MrmMsf_0102[];
+EXP (extern) lvalue char _MrmMsf_0103[];
+EXP (extern) lvalue char _MrmMsf_0104[];
+EXP (extern) lvalue char _MrmMsf_0105[];
+EXP (extern) lvalue char _MrmMsf_0106[];
+EXP (extern) lvalue char _MrmMsf_0107[];
+EXP (extern) lvalue char _MrmMsf_0108[];
+EXP (extern) lvalue char _MrmMsf_0109[];
+EXP (extern) lvalue char _MrmMsf_0110[];
