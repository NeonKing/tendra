/*
    COPYRIGHT NOTICE

    This program is the proprietary property of IXI Ltd, a subsidiary
    of the Santa Cruz Operation (SCO). Use, reproduction, production
    of amended versions and/or transfer of this program is permitted
    PROVIDED THAT:

    (a)  This legend be preserved on any such reproduction and amended
         version.

    (b)  Any recipient of such reproduction or amended version accept
         the conditions set out in this legend.

    IXI accepts no liability whatsoever in relation to any use to
    which this program may be put and gives no warranty as to the
    program's suitability for any purpose.

    All rights reserved.

    Copyright (c) 1995, 1996

*/





/* SCO CID (IXI) XmAll.h,v 1.1 1996/08/08 14:14:01 wendland Exp */


+USE "motif/1_2", "Xm/Xm.h";
+USE "motif/1_2", "Xm/ArrowB.h";  
+USE "motif/1_2", "Xm/ArrowBG.h";
+USE "motif/1_2", "Xm/AtomMgr.h";
+USE "motif/1_2", "Xm/BulletinB.h";
+USE "motif/1_2", "Xm/CascadeB.h";
+USE "motif/1_2", "Xm/CascadeBG.h";               
+USE "motif/1_2", "Xm/Command.h";
+USE "motif/1_2", "Xm/CutPaste.h"; 
+USE "motif/1_2", "Xm/DialogS.h";
+USE "motif/1_2", "Xm/Display.h";
+USE "motif/1_2", "Xm/DragDrop.h";
+USE "motif/1_2", "Xm/DrawingA.h";
+USE "motif/1_2", "Xm/DrawnB.h";
+USE "motif/1_2", "Xm/FileSB.h";
+USE "motif/1_2", "Xm/Form.h"; 
+USE "motif/1_2", "Xm/Frame.h";
+USE "motif/1_2", "Xm/Label.h";
+USE "motif/1_2", "Xm/LabelG.h";          
+USE "motif/1_2", "Xm/List.h";            
+USE "motif/1_2", "Xm/MainW.h";
+USE "motif/1_2", "Xm/MenuShell.h";
+USE "motif/1_2", "Xm/MessageB.h";
+USE "motif/1_2", "Xm/PanedW.h";
+USE "motif/1_2", "Xm/Protocols.h";
+USE "motif/1_2", "Xm/PushB.h";
+USE "motif/1_2", "Xm/PushBG.h";
+USE "motif/1_2", "Xm/RepType.h";
+USE "motif/1_2", "Xm/RowColumn.h";
+USE "motif/1_2", "Xm/Scale.h";
+USE "motif/1_2", "Xm/ScrollBar.h";
+USE "motif/1_2", "Xm/ScrolledW.h";
+USE "motif/1_2", "Xm/SelectioB.h";
+USE "motif/1_2", "Xm/SeparatoG.h";
+USE "motif/1_2", "Xm/Separator.h";
+USE "motif/1_2", "Xm/Screen.h";
+USE "motif/1_2", "Xm/Text.h";
+USE "motif/1_2", "Xm/TextF.h";           
+USE "motif/1_2", "Xm/ToggleB.h";         
+USE "motif/1_2", "Xm/ToggleBG.h";
+USE "motif/1_2", "Xm/MwmUtil.h";

