#   		 Crown Copyright (c) 1997
#   
#   This TenDRA(r) Computer Program is subject to Copyright
#   owned by the United Kingdom Secretary of State for Defence
#   acting through the Defence Evaluation and Research Agency
#   (DERA).  It is made available to Recipients with a
#   royalty-free licence for its use, reproduction, transfer
#   to other parties and amendment for any purpose not excluding
#   product development provided that any such use et cetera
#   shall be deemed to be acceptance of the following conditions:-
#   
#       (1) Its Recipients shall ensure that this Notice is
#       reproduced upon any copies or amended versions of it;
#   
#       (2) Any amended version of it shall be clearly marked to
#       show both the nature of and the organisation responsible
#       for the relevant amendment or amendments;
#   
#       (3) Its onward transfer from a recipient to another
#       party shall be deemed to be that party's acceptance of
#       these conditions;
#   
#       (4) DERA gives no warranty or assurance as to its
#       quality or suitability for any purpose and DERA accepts
#       no liability whatsoever in relation to any use to which
#       it may be put.
#
/* These values should be right */
+DEFINE N_GSYM	%% 0x20 %% ;
+DEFINE N_FNAME	%% 0x22 %% ;
+DEFINE N_FUN	%% 0x24 %% ;
+DEFINE N_STSYM	%% 0x26 %% ;
+DEFINE N_LCSYM	%% 0x28 %% ;
+DEFINE N_RSYM	%% 0x40 %% ;
+DEFINE N_SLINE	%% 0x44 %% ;
+DEFINE N_SSYM	%% 0x60 %% ;
+DEFINE N_SO	%% 0x64 %% ;
+DEFINE N_LSYM	%% 0x80 %% ;
+DEFINE N_SOL	%% 0x84 %% ;
+DEFINE N_PSYM	%% 0xa0 %% ;
+DEFINE N_ENTRY	%% 0xa4 %% ;
+DEFINE N_LBRAC	%% 0xc0 %% ;
+DEFINE N_RBRAC	%% 0xe0 %% ;
+DEFINE N_BCOMM	%% 0xe2 %% ;
+DEFINE N_ECOMM	%% 0xe4 %% ;
+DEFINE N_ECOML	%% 0xe8 %% ;
+DEFINE N_LENG	%% 0xfe %% ;
+DEFINE N_PC	%% 0x30 %% ;
