/*
    		 Crown Copyright (c) 1997
    
    This TenDRA(r) Computer Program is subject to Copyright
    owned by the United Kingdom Secretary of State for Defence
    acting through the Defence Evaluation and Research Agency
    (DERA).  It is made available to Recipients with a
    royalty-free licence for its use, reproduction, transfer
    to other parties and amendment for any purpose not excluding
    product development provided that any such use et cetera
    shall be deemed to be acceptance of the following conditions:-
    
        (1) Its Recipients shall ensure that this Notice is
        reproduced upon any copies or amended versions of it;
    
        (2) Any amended version of it shall be clearly marked to
        show both the nature of and the organisation responsible
        for the relevant amendment or amendments;
    
        (3) Its onward transfer from a recipient to another
        party shall be deemed to be that party's acceptance of
        these conditions;
    
        (4) DERA gives no warranty or assurance as to its
        quality or suitability for any purpose and DERA accepts
        no liability whatsoever in relation to any use to which
        it may be put.
*/



/**********************************************************************
$Author: pwe $
$Date: 1998/03/15 16:00:23 $
$Revision: 1.6 $
$Log: target_v.h,v $
 * Revision 1.6  1998/03/15  16:00:23  pwe
 * regtrack dwarf dagnostics added
 *
 * Revision 1.5  1998/03/11  11:03:05  pwe
 * DWARF optimisation info
 *
 * Revision 1.4  1998/02/18  11:22:06  pwe
 * test corrections
 *
 * Revision 1.3  1998/02/02  11:18:26  pwe
 * 80x86 shape_overlap correction
 *
 * Revision 1.2  1998/01/21  10:30:00  pwe
 * labdiff change
 *
 * Revision 1.1.1.1  1998/01/17  15:55:52  release
 * First version to be checked into rolling release.
 *
 * Revision 1.85  1998/01/11  15:27:14  pwe
 * v5r7
 *
**********************************************************************/


#define target_version  5
#define target_revision  12
