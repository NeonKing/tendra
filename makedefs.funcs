# $Id$
# ex:filetype=sh

# Wrap normal command utilities with env.
AWK="/usr/bin/env awk"
CUT="/usr/bin/env cut"
SED="/usr/bin/env sed"


# Check if the CC environment variable is set.
check_cc()
{
	if [ -z $CC ]; then
		CC="cc";
	elif [ "$CC" = "CC" ]; then
		CC="cc";
		echo "NOTICE: CC is often the C++ compiler, we just need a normal C compiler,";
		echo "        setting CC to \"cc\"."
		echo;
	elif [ "$CC" = "xlc_r" -o "$CC" = "xlc_r4" -o "$CC" = "xlc_r7"\
		-o "$CC" = "cc_r" -o "$CC" = "cc_r4" -o "$CC" = "cc_r7" ]; then
		# We are on AIX, and since we focus on ANSI as a compiler, use
		# the right bootstrapping compiler: xlc (which implies ansi).
		CC="xlc";
		CCTYPE="AIX";
		echo "NOTICE: Using xlc_r or cc_r creates POSIX threaded applications.  We do";
		echo "        not need that.  Setting CC to \"xlc\"."
		echo
	fi

	check_cc_ver ${CC}
}

# Check to see if we can lift the version string from the cc call.
check_cc_ver()
{
	CC=$1

	case "${CC}" in
		cc*)
		VERSIONSTRING=`${CC} --version`
		if [ $? -eq 0 ]; then
			CCVER=`echo ${VERSIONSTRING} | ${AWK} '{ where = match($0, /[[:digit:]][[:digit:]]?\.?[[:digit:]]?[[:digit:]]?\.[[:digit:]][[:digit:]]?/); if (where != 0) print substr($0, RSTART, RLENGTH); else print where }'`
		fi
		;;
		[gi]cc*)
		VERSIONSTRING=`${CC} --version`
		if [ $? -eq 0 ]; then
			CCVER=`echo ${VERSIONSTRING} | ${AWK} '{ where = match($0, /[[:digit:]][[:digit:]]?\.?[[:digit:]]?[[:digit:]]?\.[[:digit:]][[:digit:]]?/); if (where != 0) print substr($0, RSTART, RLENGTH); else print where }'`
		fi
		;;
		*)
			CCVER=0
		;;
	esac

	CCVER_MAJOR=`echo ${CCVER} | ${CUT} -d . -f 1`
	CCVER_MINOR=`echo ${CCVER} | ${CUT} -d . -f 2`
	CCVER_TINY=`echo ${CCVER} | ${CUT} -d . -f 3`
	if [ -z ${CCVER_TINY} ]; then
		CCVER_TINY=0
	fi
}

# Check to see which architecture the host is running on.
check_hostarch()
{
	case "$HOSTARCH" in
		alpha)
		# DEC, nay, Compaq, nay HP Alpha
		BLDARCH="alpha"
		BLDARCHBITS="64"
		;;
		amd64)
		# AMD64
		BLDARCH="amd64"
		BLDARCHBITS="64"
		;;
		i386)
		# Intel 80386
		BLDARCH="80x86"
		BLDARCHBITS="32"
		;;
		i486)
		# Intel 80486
		BLDARCHBITS="32"
		;;
		i586)
		# Intel 80586 (Pentium, Pentium MMX)
		BLDARCH="80x86"
		BLDARCHBITS="32"
		;;
		i686)
		# Intel 80686 (Pentium Pro, Pentium II,
		# Pentium III, Pentium 4, AMD Athlon)
		BLDARCH="80x86"
		BLDARCHBITS="32"
		;;
		sparc)
		# SPARC 32 bit
		BLDARCH="sparc"
		BLDARCHBITS="32"
		;;
		sparc64)
		# SPARC 64 bit
		BLDARCH="sparc"
		BLDARCHBITS="64"
		;;
		*)
		echo "Unknown architecture \"${HOSTARCH}\""
		echo "Please report the following to the developers:"
		echo
		echo "uname -m:	${HOSTARCH}"
		echo
		;;
	esac
}

usage()
{
	echo "makedefs"
	echo "usage: [OPTIONS] ./makedefs [-h]"
	echo
	echo "Configuration definition generator of the TenDRA Project"
	echo
	echo "Available OPTIONS (OPTION=value):"
	echo "PREFIX		installation prefix"
	echo "CC		C compiler command name"
	echo "CFLAGS		C compiler flags"
	echo "EXECFORMAT	desired default executable format (aout, elf)"
}
