TENDRA 5.0.0 INSTALLATION INFORMATION
=====================================

REVISION INFORMATION
--------------------
$Id$

COPYRIGHT INFORMATION
---------------------

Please read the file COPYRIGHT for the copyright notice.


DOWNLOADING INFORMATION
-----------------------

The TenDRA 5.0.0 release is available via the Web site:

  http://www.tendra.org/download/releases/tendra/

or by FTP from ftp.allbsd.org, in the directory /pub/TenDRA/releases/tendra/.
It consists of the gzipped tar archive:

	TenDRA-5.0.0.tar.gz

(about 3.9MB - see the actual site for the precise figure), plus the
optional documentation, comprising a copy of the documents available
from the TenDRA web site:

	TenDRA-5.0.0-doc.tar.gz

(about 760kB).


INSTALLATION INFORMATION
------------------------

TenDRA has a few dependencies, of which only one would most likely be missing
on any decent Unix or Unix-like system:

ash,
awk,
sed
Simon J. Gerraty's bmake [http://www.crufty.net/help/sjg/bmake.html]
Used version: http://www.crufty.net/ftp/pub/sjg/bmake-20060728.tar.gz

The main source archive can be extracted using:

pax.bz2:	bzip2 -dc TenDRA-<version|date>.pax.bz2 | pax -rv
pax.gz:		gzip -dc TenDRA-<version|date>.pax.gz | pax -rv
tar.bz2:	bzip2 -dc TenDRA-<version|date>.tar.bz2 | tar xvf -
tar.gz:		gzip -dc TenDRA-<version|date>.tar.gz | tar xvf -

to give a directory, TenDRA-<version|date>, containing the release source.
If you also want to install the release documentation you will also need to
download TenDRA-5.0.0-doc.tar.gz and extract this as above.  The
documentation is extracted into the subdirectory TenDRA-5.0.0/doc.

The release is installed by running the shell script 'makedefs' found in
the main source directory.  The default configuration installs the
public executables into /usr/local/bin, the private executables,
libraries, configuration files etc. into /usr/local/lib/TenDRA, and
the manual pages into /usr/local/man.  This location and other options  may
be changed by passing along variables to the 'makedefs' script (which is
fully commented).
For example:

	$ CC=gcc ./makedefs

will configure the build to use gcc as the compiler.  Follow this up with
executing make (or bmake if called like that):

	$ make -DBOOTSTRAP

When compilation is done follow up with:

	$ make

And when all is done use the following to install:

	$ make install


KNOWN INSTALLATION PROBLEMS
---------------------------

Several of the components have been updated to use the posix1 or xpg4
APIs rather than the older posix and xpg3.  Due to an error in the
TenDRA-4.1.1 posix1 API specification, this means that they may not
compile under the TenDRA-4.1.1 (although they will with TenDRA-4.1.2
of course).  If this is an issue then modify the Makefiles to use
the older APIs.

The location of certain system linker files has changed in later versions
of Linux.  The solution is to change the tcc environment files to reflect
the new locations.

Todo:	cd src/lib/env/linux/elf
	mv 80x86 80x86.old
	mv 80x86.new 80x86
